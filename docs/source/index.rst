.. KBA_sawtooth documentation master file, created by
   sphinx-quickstart on Wed Sep 12 12:07:01 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KBA_sawtooth's IIITM-K!
========================================
./cn.jpg
************
Introduction
************
Hyperledger Sawtooth is an enterprise blockchain platform for building
distributed ledger applications and networks. The design philosophy targets
keeping ledgers *distributed* and making smart contracts *safe*, particularly
for enterprise use.

Sawtooth simplifies blockchain application development by separating the core
system from the application domain. Application developers can specify the
business rules appropriate for their application, using the language of their
choice, without needing to know the underlying design of the core system.

Sawtooth is also highly modular. This modularity enables enterprises and
consortia to make policy decisions that they are best equipped to make.
Sawtooth's core design allows applications to choose the transaction rules,
permissioning, and consensus algorithms that support their unique business
needs.

Sawtooth is an open source project under the Hyperledger umbrella. For
information on how to contribute, see `Join the Sawtooth Community`_

Bitcoin
========================================

Blockchain technology was first introduced with the release of the Bitcoin White Paper
by Satoshi Nakamoto in 2008. The Bitcoin blockchain was designed to create a peer-to-
peer electronic cash system with no central issuer. Approximately every 10 minutes, the
nodes on the network come to consensus on a set of unspent coins, and the conditions
required to spend them. This data set, known as the ‘unspent transaction output’, or UTXO,
set, can be modified by submitting transactions to the network that replace one or more
UTXOs with a new set of unspent transaction outputs. In order to ensure that all nodes
on the network come to consensus on this dataset, the Bitcoin protocol leverages a set
of transaction validation rules and a novel consensus mechanism known as proof-of-work
which allows for permissionless and anonymous participation in the consensus protocol.

Ethereum
========================================

In 2013, Vitalik Buterin published a White Paper that expanded on the blockchain concept
by viewing the Bitcoin protocol as a state transition system. State machine replication is a
technique used to create fault tolerant services through data replication and consensus,
and was first introduced by Leslie Lamport in 1984. The goal of the Ethereum protocol was
to create a ‘decentralized application platform’ that would enable developers to deploy ‘smart
contracts’ that would run on the Ethereum state machine. ‘Smart contracts’ are state transition
functions that take the current state of the ledger and a transaction, and produce a new
ledger state. These ‘smart contracts’ could be used to not only dictate the logic required
for the transfer of Ethereum’s native currency, Ether, but could also be used to manage
arbitrary key-value stores. These key-value stores are most often used to represent the
ownership of other assets such as financial securities, land titles, and domain names.

Distributed Ledgers
========================================

Since the release of Ethereum, a variety of other ‘distributed ledgers’ implementations have
been created to meet the needs of the enterprise. These distributed ledger implementations
include software that expands upon existing protocols (e.g. Bitcoin - MultiChain, Chain,
Blockstream and Ethereum - Quorum) and creates entirely new implementations (Corda
and Fabric). These implementations deviate from public blockchains to address limitations
in existing protocols such as throughput, security, efficiency, usability, and confidentiality.
Some distributed ledgers differ substantially from their blockchain predecessors as a result
of changes to the underlying networking, journalling layer, smart contract, and consensus
layers; some even going so far as to not include ‘blocks’ or a ‘chain’.

Related Technologies
========================================

*Peer-to-peer networking
*Event driven databases
*Replicated state machines
	
*Distributed databases
*Replicated logs
*Consensus algorithms


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
